============
Printing
============

Printing in sqlkit uses the ``oootemplate`` module that is distributed along
with sqlkit but could be used in a totally independent way and does not
depend in any way on other parts of sqlkit. 

.. automodule:: sqlkit.misc.oootemplate


