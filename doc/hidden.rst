.. toctree::
   :maxdepth: 3

   sqlkit/contents
   misc/tour

.. toctree::
   :maxdepth: 3

   misc/sqledit

.. toctree::
   :maxdepth: 2

   misc/contents
   layout/contents
   debug/contents
..   sqlkit/advanced

.. toctree::
   :maxdepth: 1

   misc/tutorials

