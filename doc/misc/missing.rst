======================================
 What's Missing and how to contribute
======================================



Well, this is a pretty large chapter... there are tons of things that are
missing from this package and that are really needed but I want to focus on
the following:

  - saving queries
  - good field render for CellRenderer (notably TEXT fields and DateTime)
  - good system to warn people of all orphans when deleting
  - integrate a permission system

I'd be happy to accept any suggestion, patch or contributed feature on
anything that can make sqlkit more powerful.


sandro





.. _PyGtkMVC: http://pygtkmvc.sourceforge.net/index.php

