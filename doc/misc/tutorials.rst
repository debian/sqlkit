Mailing list
============

You can join our mailing list_

Tutorials
=========

.. toctree::
   :maxdepth: 2

   tutorial
   sqledit
   tour


.. _list: http://groups.google.com/group/sqlkit
