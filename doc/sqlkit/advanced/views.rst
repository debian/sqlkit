.. automodule:: sqlkit.widgets.table.modelproxy

.. _view_class:
    
View
=====

.. automodule:: sqlkit.widgets.table.columns
   
