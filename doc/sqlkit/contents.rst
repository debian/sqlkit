=======
SQLKit
=======

.. sidebar:: Release

   This documentation is relative to the **last mercurial release**.
   Documentation for your release is in :file:`doc` directory

.. toctree::
   :maxdepth: 2

   widgets
   browsing
   editing
   advanced/contents
   ../printing/contents
