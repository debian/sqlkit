==============
 Browsing data
==============

.. toctree::
   :maxdepth: 2


   constraints
   filters
   totals
   localization
