Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sqlkit
Source: http://sqlkit.argolinux.org/misc/download.html

Files: *
Copyright: © 2005-2014 Sandro Dentella <sandro@e-den.it>
License: GPL-3+

Files: debian/*
Copyright: © 2010-2015 Pietro Battiston <me@pietrobattiston.it>
License: GPL-3+

Files: sqlkit/layout/dateedit.py
Copyright: © 2005 Fabian Sturm, © 2009 Sandro Dentella
License: LGPL-2.1

Files: doc/html/_static/*.js doc/html/_static/default.css doc/html/_static/basic.css
Copyright: © 2007-2008 Georg Brandl <georg@python.org>
                       Armin Ronacher <armin.ronacher@active-4.com>
                       Josip Dzolonga,
                       Gerold Penz,
                       Vivake Gupta <v@nano.com>
License: BSD-2-clause

Files: doc/html/_static/jquery.js
Copyright: Copyright © 2009 John Resig, The Dojo Foundation
License: MIT or GPL-2

Files: doc/html/_static/underscore.js
Copyright: © 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
License: MIT

Files: doc/html/_static/imgpreview.js debian/missing-sources/imgpreview.min.0.22.jquery.js
Copyright: © 2009 James Padolsey
License: MIT or GPL-3+

Files: doc/html/_static/slides.min.jquery.js
Copyright: © 2011 Nathan Searles
License: MIT

Files: doc/static/AnythingSlider/* doc/html/_static/AnythingSlider/*
Copyright: © 2007 George Smith
License: MIT

Files: doc/html/_static/sqlkit.css
Copyright: © 2009 Armin Ronacher, Georg Brandl, Sandro Dentella
License: MIT or GPL-3+

Files: doc/html/_static/jMenu.jquery.* doc/static/jMenu.jquery.*
Copyright: © 2010 Surrel Mickael, Croissance Net
License: MIT

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 or later.
 .
 On Debian systems, the complete text of the GNU General Public License can be
 found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.1
 This widget is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/GPL-2.1'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 2.
 .
 On Debian systems, the complete text of the GNU General Public License can be
 found in `/usr/share/common-licenses/GPL-2'.
